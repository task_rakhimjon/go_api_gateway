package client

import (
	"go_api_gateway/config"
	"go_api_gateway/genproto/integration_service"
	"go_api_gateway/genproto/storage_service"

	"google.golang.org/grpc"
)

type ServiceManagerI interface {
	IntegrationSevice() integration_service.IntegrationServiceClient
	StorageService() storage_service.StorageServiceClient
}

type grpcClients struct {
	integrationService integration_service.IntegrationServiceClient
	storageService     storage_service.StorageServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	connIntegrationService, err := grpc.Dial(
		cfg.IntegrationServiceHost+cfg.IntegrationGRPCPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connStorageService, err := grpc.Dial(
		cfg.StorageServiceHost+cfg.StorageGRPCPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		integrationService: integration_service.NewIntegrationServiceClient(connIntegrationService),
		storageService:     storage_service.NewStorageServiceClient(connStorageService),
	}, nil

}

func (g *grpcClients) IntegrationSevice() integration_service.IntegrationServiceClient {
	return g.integrationService
}

func (g *grpcClients) StorageService() storage_service.StorageServiceClient {
	return g.storageService
}
