package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	HTTPPort   string
	HTTPScheme string

	DefaultOffset string
	DefaultLimit  string

	// Integration Service
	IntegrationServiceHost string
	IntegrationGRPCPort    string

	// Storage Service
	StorageServiceHost string
	StorageGRPCPort    string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("/app/.env"); err != nil {
		if err := godotenv.Load("./.env"); err != nil {
			fmt.Println("No .env file found")
		}
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "go_api_gateway/"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8080"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	// Integration service
	config.IntegrationServiceHost = cast.ToString(getOrReturnDefaultValue("INTEGRATION_SERVICE_HOST", "0.0.0.0"))
	config.IntegrationGRPCPort = cast.ToString(getOrReturnDefaultValue("INTEGRATION_GRPC_PORT", ":9101"))

	// Storage Service
	config.StorageServiceHost = cast.ToString(getOrReturnDefaultValue("STORAGE_SERVICE_HOST", "0.0.0.0"))
	config.StorageGRPCPort = cast.ToString(getOrReturnDefaultValue("STORAGE_SERVICE_PORT", ":9102"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
