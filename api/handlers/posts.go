package handlers

import (
	"context"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"

	"go_api_gateway/api/http"
	"go_api_gateway/genproto/integration_service"
)

// posts godoc
// @ID save-posts
// @Router /post [POST]
// @Summary Save Bulk Posts
// @Description Save Bulk Posts
// @Tags integration-service
// @Accept json
// @Produce json
// @Success 200 {object} http.Response{data=integration_service.SavePostsResponse} "Response Save Posts"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpsertBulkPosts(c *gin.Context) {

	ctx, cancel := context.WithTimeout(c.Request.Context(), 5*time.Minute)
	defer cancel()

	emptyPost := []*integration_service.Post{}
	response, err := h.services.IntegrationSevice().UpsertBulkPosts(ctx, &integration_service.UpsertBulkPostsRequest{
		Posts: emptyPost,
	})

	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, response)
}

// posts godoc
// @ID get-post-data
// @Router /post/{guid} [GET]
// @Summary Get post data by its guid
// @Description Get post data by its guid
// @Tags integration-service
// @Accept json
// @Produce json
// @Param guid path string true "guid"
// @Success 200 {object} http.Response{data=integration_service.GetPostByIdResponse} "GetPostByIdResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPostById(c *gin.Context) {

	ctx, cancel := context.WithTimeout(c.Request.Context(), 7*time.Second)
	defer cancel()

	resp, err := h.services.IntegrationSevice().GetPostById(ctx, &integration_service.GetPostByIdRequest{
		Guid: c.Param("guid"),
	})
	fmt.Println("test added")
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// posts godoc
// @ID get-all-post-data
// @Router /post [GET]
// @Summary returns get-all-post-data
// @Description this returns get-all-post-data
// @Tags integration-service
// @Accept json
// @Param search query string true "search by posts` body && title"
// @Param limit query int false "limit"
// @Param page query int false "page"
// @Produce json
// @Success 200 {object} http.Response{data=integration_service.GetPostResponse} "GetPostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAllPosts(c *gin.Context) {
	var (
		limit, page int
	)

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.BadRequest, "invalid limit parameter")
		return
	}
	page, err = h.getPageParam(c)
	if err != nil {
		h.handleResponse(c, http.BadRequest, "invalid page parameter")
		return
	}

	posts, err := h.services.IntegrationSevice().GetAllPosts(c.Request.Context(), &integration_service.GetAllPostsRequest{
		Search: c.Query("search"),
		Limit:  int64(limit),
		Page:   int64(page),
	})
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, posts)
}
