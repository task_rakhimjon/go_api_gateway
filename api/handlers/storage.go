package handlers

import (
	"context"
	"go_api_gateway/api/http"
	"go_api_gateway/genproto/storage_service"
	"go_api_gateway/pkg/util"
	"time"

	"github.com/gin-gonic/gin"
)

// UpdatePost godoc
// @ID update_post
// @Router /storage [PUT]
// @Summary Update post
// @Description Update post
// @Tags storage-service
// @Accept json
// @Produce json
// @Param user-info-field body storage_service.UpdateRequest true "UpdateRequest"
// @Success 200 {object} http.Response{data=storage_service.StoragePost} "StoragePost data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Update(c *gin.Context) {
	var update_info storage_service.UpdateRequest

	err := c.ShouldBindJSON(&update_info)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(c.Request.Context(), 5*time.Minute)
	defer cancel()

	resp, err := h.services.StorageService().Update(ctx, &update_info)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// RemovePost godoc
// @ID delete_post
// @Router /storage [DELETE]
// @Summary Remove PostInfo
// @Description Get PostInfo
// @Tags storage-service
// @Accept json
// @Produce json
// @Param post-guid path string true "post-guid"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Delete(c *gin.Context) {
	postGuid := c.Param("post-guid")

	if !util.IsValidUUID(postGuid) {
		h.handleResponse(c, http.InvalidArgument, "post-guid field id is an invalid uuid")
		return
	}

	resp, err := h.services.StorageService().Delete(
		c.Request.Context(),
		&storage_service.GetByIdRequest{
			Guid: postGuid,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// posts godoc
// @ID get-storage-post-data
// @Router /storage/{guid} [GET]
// @Summary Get post data by its guid
// @Description Get post data by its guid
// @Tags storage-service
// @Accept json
// @Produce json
// @Param guid path string true "guid"
// @Success 200 {object} http.Response{data=storage_service.GetByIdResponse} "GetByIdResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetById(c *gin.Context) {

	ctx, cancel := context.WithTimeout(c.Request.Context(), 7*time.Second)
	defer cancel()

	resp, err := h.services.StorageService().GetById(ctx, &storage_service.GetByIdRequest{
		Guid: c.Param("guid"),
	})
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// posts godoc
// @ID get-storage-all-post-data
// @Router /storage [GET]
// @Summary returns get-all-post-data
// @Description this returns get-all-post-data
// @Tags storage-service
// @Accept json
// @Param search query string true "search by posts` body && title"
// @Param limit query int false "limit"
// @Param page query int false "page"
// @Produce json
// @Success 200 {object} http.Response{data=integration_service.GetPostResponse} "GetPostResponse"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAll(c *gin.Context) {
	var (
		limit, page int
	)

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.BadRequest, "invalid limit parameter")
		return
	}
	page, err = h.getPageParam(c)
	if err != nil {
		h.handleResponse(c, http.BadRequest, "invalid page parameter")
		return
	}

	posts, err := h.services.StorageService().GetAll(c.Request.Context(), &storage_service.GetAllRequest{
		Search: c.Query("search"),
		Limit:  int64(limit),
		Page:   int64(page),
	})
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, posts)
}
