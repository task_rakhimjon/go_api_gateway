module go_api_gateway

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/cast v1.4.1
	github.com/streamingfast/logging v0.0.0-20220222131651-12c3943aac2e
	github.com/swaggo/gin-swagger v1.3.3
	github.com/swaggo/swag v1.8.0 // indirect
	go.uber.org/zap v1.19.1
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.28.0
)
